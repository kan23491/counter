import React, { useState, useEffect} from 'react';
import { Layout } from 'antd';
import { Link} from 'react-router-dom';
import '../App.css';
import logo from '../imge/Logo.png'
import 'antd/dist/antd.css';
import {Breadcrumb,Menu } from 'antd';
import { HomeOutlined,UserOutlined} from '@ant-design/icons';



const Counter =() =>{
  const {Header} = Layout;
  const [time, setTime] = useState();

  useEffect((time) => {
    setTime(time = new Date().toLocaleTimeString('en-US',{hour: 'numeric', minute: 'numeric', hour12: true }))
  },[],1000)

  const charts = (
    <Menu>
      <Menu.Item >
       <Link to="/shatter">SCATTER PLOTS</Link>
      </Menu.Item>
      <Menu.Item >
        <Link to="/line"> LINE CHARTS</Link>
      </Menu.Item>
      <Menu.Item >
      <Link to="/bar">BAR CHARTS</Link>
      </Menu.Item>
      <Menu.Item >
      <Link to="/pie"> PIE CHARTS</Link>
      </Menu.Item>
    </Menu>
  )

  return (
    <div> 
      <Layout>
        <Header style={{backgroundColor:'#FFF',borderBottom:'2px solid #DBD9DC',height:'100px'}}>
          <div className="header">
            <img src={logo} alt={"logo"} style={{width:'20%', height:'20%',paddingTop:'20px'}}/> 
            <Breadcrumb style={{margin:"20px"}}>
              <Breadcrumb.Item>
                <HomeOutlined />
                <Link to="/" className="text">Home</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <UserOutlined />
                <Link to="/about" className="text">About us</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/counter" className="text">Counter</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/contact" className="text">Contact</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item overlay={charts}>
                <Link to="/plotly" className="text">Plotly</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <p className="time">{time}</p>
          </div>
        </Header>
      </Layout>
    </div>
  )
}

export default Counter;