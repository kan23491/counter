import React from 'react';

import { Row, Col } from 'antd';
import { Card } from 'antd';

const GridCrad = (props) => {
    return (
        <div>
          <div>
          {props.children}
        </div>
        <Row className="gridRow">
          <Col className="colCard">
            <Card title="how to increease Counter" bordered={false} style={{ width: 300, }}>
              <p>
                you add number at Steps and click butotn increease 
                after number box counter show where you click
              </p>
            </Card>
          </Col>
          <Col className="colCard" >
            <Card title="how to increease Counter" bordered={false} style={{ width: 300 }}>
              <p>  you add number at Steps and click butotn Reduce 
                after number box counter show where you click </p>
            </Card>
          </Col>
          <Col className="colCard">
            <Card title="how to clear number for counter" bordered={false} style={{ width: 300 }}>
              <p>you can click butotn clear after number in box counter is default  </p>
            </Card>
          </Col>
          <Col className="colCard">
            <Card title="how to reset counter and steps" bordered={false} style={{ width: 300 }}>
              <p>you can click butotn reset after number in box counter and Steps is default</p>
            </Card>
          </Col>
        </Row>
      
      </div>
        
    )
}

export default GridCrad;