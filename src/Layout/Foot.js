import React from 'react';
import { Layout } from 'antd';
import {  CopyrightOutlined } from '@ant-design/icons';

const Foot = () => {
    const {Footer} = Layout;
    return (
      <Footer >
        <div className="footer">
          <div className="icon"> 
            <CopyrightOutlined/> 
          </div>
          <p>2 0 2 0 T R A D I T I O N B R O K E R S ( T H A I L A N D )   C O . , L T D .</p>
        </div>
        <div className="footer">
          <p>A L L  R I G H T R E S E R V  D .</p>
        </div>
      </Footer>
    )
}

export default Foot;