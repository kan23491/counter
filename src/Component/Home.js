import React from 'react';
import '../App.css';
import 'antd/dist/antd.css';
import { Card,Tabs  } from 'antd';
import { Link } from 'react-router-dom';
import {Table } from 'antd';


const { TabPane } = Tabs;

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    width: 100,
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
]

const data = [];
for (let i = 0; i < 100; i++) {
  data.push({
    key: i,
    name: `Edward King ${i}`,
    age: 32,
    address: `London, Park Lane no. ${i}`,
  });
}

const  Home =() =>{
  return (
      <div >
        <div className="tabs">
          <Tabs defaultActiveKey="1">
            <TabPane tab="what is ReactJS" key="1">
              <h3>what is ReactJS</h3>
              <p>Nowadays, the amount of Javascript frameworks and libraries are insane. Once I read in Twitter from a guy who was playing with his co workers to say a name or a word, and if there exists a Javascript framework named like that, the person has to drink a shot. Hardcore.
              But today I’m not going to talk about all of them but one that has caught my attention in the past month: ReactJS.</p>
            </TabPane>
            <TabPane tab="Where does it come from?" key="2">
              <h3>Where does it come from?</h3>
              <p>React is a Javascript library created by Jordan Walke, a Facebook engineer. It’s pretty new, it only has 4 years since its first release, and there is a huge community working hard in order to make it better in every release.</p>
            </TabPane>
            <TabPane tab="What can I do with React?" key="3">
              <h3>What can I do with React?</h3>
              <p>If you know about design patterns, you might have listened about the MVC pattern (Model-View-Controller). If not, all you have to now right now is that React works as the V in MVC. And what does it mean? you might be asking yourself. Well, with React, you can design all the front-end part of your application. That means, you can create easily all the user interface of your application.</p>
            </TabPane>
          </Tabs>
        </div>
        <div className="disacription">
        <div className="cardHome">
           <Card title="TRADITION ASIA" extra={<Link to="/about"> More</Link>} style={{ width: 400,marginRight:'50px' }}>
            Tradition is the interdealer broking arm of Compagnie Financière Tradition and one of the world's 
            largest interdealer brokers in over-the-counter financial and commodity related products. 
            Represented in over 28 countries, Compagnie Financière Tradition is listed on the Swiss stock exchange.
          </Card>
          <div style={{width:"50%"}}>
            <Table columns={columns} dataSource={data} pagination={{ pageSize: 10 }} scroll={{ y: 200 }} />
          </div>
        </div>
        </div>

      </div>
  )
}

export default Home;