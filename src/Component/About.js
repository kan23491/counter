import React from 'react';
import '../App.css';
import { Card } from 'antd';
import tradition from '../imge/check.jpg';
import location from '../imge/location.png';

const { Meta } = Card;

const About = () => {
    return(
       <div className="cardAbout">
            <Card
                hoverable
                style={{ width: 400, marginRight:"50px"}}
                cover={<img alt="example" src={tradition} />}
            >
                <Meta 
                    title="About us" 
                    description="Tradition is the interdealer broking arm of Compagnie Financière Tradition 
                    and one of the world's largest interdealer brokers in over-the-counter financial and commodity related products. 
                    Represented in over 28 countries, Compagnie Financière Tradition is listed on the Swiss stock exchange. " 
                />
            </Card>

            <Card
                hoverable
                style={{ width: 450 }}
                cover={<img alt="example" src={location} style={{height:"270px"}} />}
            >
                <Meta title="Location" description="Nimmanhaemin Mueang Chiang Mai District, Chiang Mai 50200" />
            </Card>
       </div>
    )
}

export default About;