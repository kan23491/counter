import React from 'react';
import Plot from 'react-plotly.js';


const Poltly = () => {
    return(
        <div className="allPolt">  
        
            <Plot
                data={[
                    {
                        x: [1,2,3,4,5,6],
                        y: [22,23,24,22,25,22],
                        type:'scatter',
                        mode:'markers',
                        marker:{size:10, color:'red'},
                        name:'Age',
                        showlegend:true
                        
                    },
                ]}
                layout={{
                    width: 500, 
                    height: 400, 
                    title:'THe Ages of students',
                    hovermode:'closest',
                }}
            />

            <Plot
                data={[
                    {
                        x: [1,2,3,4,5,6],
                        y: [22,23,25,22,25,22],
                        type:'bar',
                        marker:{color:'blue'},
                        name:'Age'
                    },
                    {
                        x: [1,2,3,4,5,6],
                        y: [53,49,63,50,72,69],
                        type:'bar',
                        marker:{color:'red'},
                        name:'weigth'
                    }
                ]}
                layout={{
                    width: 500, 
                    height: 400, 
                    title:'THe Growth of students',
                    hovermode:'closest',
                }}
            />

            <Plot
                data={[
                    {
                        x: [1,2,3,4,5,6],
                        y: [53,49,63,50,72,69],
                        type:'line',
                        marker:{color:'blue'},
                        name:'Weigth'
                    },
                    {
                        x: [1,2,3,4,5,6],
                        y: [22,23,25,22,25,22],
                        type:'line',
                        marker:{color:'red'},
                        name:'age'
                    },
                ]}
                layout={{
                    width: 500, 
                    height: 400, 
                    title:'THe Growth of students',
                    hovermode:'closest',
                }}
            />

            <Plot
                data={[
                    {
                        values: [20,60,20],
                        labels: ['weight <= 49', 'weight <= 69', 'weight > 69'],
                        type:'pie',
                    }
                ]}
                layout={{
                    width: 450, 
                    height: 400, 
                    title:'THe Growth of students',
                    hovermode:'closest',
                }}
            />

        </div>
    
    )
}
export default Poltly;