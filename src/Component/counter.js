import React, { useState} from 'react';
import { Layout } from 'antd';
import '../App.css';
import 'antd/dist/antd.css';
import { Input, InputNumber, Button } from 'antd';
import { DollarOutlined, PlusCircleOutlined, CloseCircleOutlined, UndoOutlined } from '@ant-design/icons';

const Counter =() =>{
  const {Content} = Layout;
  const counte = 0;
  const steps = 1 ;
  const [showCount, setClick] = useState(counte);
  const [showSteps, setSteps] = useState(steps);

  const handleIncrease = () => {

    setClick(showCount + showSteps)
  }

  const handleReduce = () =>{
    setClick(showCount - showSteps)
  }

  const clickSteps =(value) => {
    for(let i =1; i<= value;i++) {
      setSteps(value)
    }
  }

  const clickClear = () => {
    setClick(counte)
  }

  const clickReset = () => {
    setClick(counte)
    setSteps(steps)
  }

  const styleSteps = {
    paddingLeft:'35px',
    with:'10%'
  }
 
  return (
    <div> 
      <Content style={{backgroundColor:'#FFFFFF', height:'300px',padding:'100px'}}> 
        <div className="conter">
          <div className="counter">
            <p>Counter</p>
            <div>
              <Input 
                type="text"  
                value={showCount} 
                prefix={<DollarOutlined />} 
                
              />
            </div>
          </div>
          <div className="steps">
            <p>Steps</p>
            <InputNumber
              style={styleSteps}
              size="1" 
              onChange={clickSteps}
              defaultValue={1}
              value={showSteps}
            /> 
          </div>
        </div>
        <div className="btn">
          <Button 
            type="primary" 
            icon={<PlusCircleOutlined />} 
            onClick={handleIncrease}
          >
            Increase
          </Button>
          <Button 
            type="danger"  
            style={{marginLeft:'10px'}} 
            icon={<PlusCircleOutlined />} 
            onClick={handleReduce}
          >
            Reduce
          </Button>
          <Button 
            danger  
            style={{marginLeft:'10px'}} 
            icon={<CloseCircleOutlined  />} 
            onClick={clickClear}
          >
            Clear
          </Button>
          <Button  
            style={{marginLeft:'10px'}} 
            icon={<UndoOutlined  />}
            onClick={clickReset}
          >
            Reset
          </Button>
        </div>
      </Content>
    </div>
  )
}

export default Counter;