import React from 'react';
import '../App.css';
import { Form, Input, Button,Select } from 'antd';

const { Option } = Select;

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
      </Select>
    </Form.Item>
  );

const Contant = () => {
  
    return(
      <div>
        <Form className="fromInput" style={{margin:"50px"}}>
            <Form.Item
                className="inputContact"
                name="email"
                label="E-mail"
                rules={[
                {
                    type: 'email',
                    message: 'The input is not valid E-mail!',
                },
                {
                    required: true,
                    message: 'Please input your E-mail!',
                },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                className="inputContact"
                name="name"
                label="Name"
                rules={[{ required: true, message: 'Please input your name!', whitespace: true }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                className="inputContact"
                name="phone"
                label="Phone Number"
                rules={[{ required: true, message: 'Please input your phone number!' }]}
            >
                <Input addonBefore={prefixSelector} />
            </Form.Item>
            <Form.Item  >
                <Button type="primary" htmlType="submit" className="btnSend">
                    Send
                </Button>
            </Form.Item>
        </Form>
      </div>
    )
}

export default Contant;