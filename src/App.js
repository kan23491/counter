import React from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.css';
import 'antd/dist/antd.css';

//router
import Home from './Component/Home';
import About from './Component/About';
import Counter from './Component/counter';
import Contact from './Component/Contect';
import Poltly from  './Component/plotly';
//polt
import shatter from './Polt/shatterPolts'
import line from './Polt/lineCharts';
import bar from './Polt/BarCharts';
import pie from './Polt/pieCharts';

//layout
import header from './Layout/Header';
import footer from './Layout/Foot';
import GridCrad from './Layout/gridCrad';

const GridLayout = ({component:Component, ...rest}) => (
  <Route {...rest} render= {props => (
   <GridCrad>
     <Component {...rest} />
   </GridCrad>
  )}></Route>
)
const AppReute = ({component:Component,layout:Layout, ...rest}) => (
  <Route {...rest} render= {props => (
    <Layout>
      <Component {...props}></Component>
    </Layout>
  )}></Route>
)

const App =() =>{

  return (
   <div>  
    <BrowserRouter>
    <AppReute layout={header} component={header}/>
      <Switch>
        <Route path="/plotly" component={Poltly} />
        <Route path="/shatter" component={shatter} />
        <Route path="/bar" component={bar} />
        <Route path="/line" component={line} />
        <Route path="/pie" component={pie} />
        <Route path="/about" component={About} />
        <GridLayout  path="/counter"  component={Counter} />
        <Route path="/contact" component={Contact} />
        <Route path="/" component={Home} />
      </Switch>
    <AppReute layout={footer} component={footer} />
    </BrowserRouter>
    </div>
  )
}

export default App;