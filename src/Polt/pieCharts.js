import React from 'react';
import Plot from 'react-plotly.js';
import '../App.css';

const allLable = ['amount','man','woman'];
const allValues = [
    [6,2,4],
    [22,5,17],
    [8,1,7],
    [6,2,4]
];
const allColor = [
    ['#1D93EF','#5CADFA','#3BDBFA'],
    ['green','#926B10','#EFBB1D'],
    ['#0D09B1','#2C28D2 ','#6460F8 '],
    ['pink','#7F60F8 ','red']
]

const PieCharts = () => {
    return (
        <div className="pie">
               <Plot
                    data={[
                        {
                            values: allValues[0],
                            labels: allLable,
                            type:'pie',
                            name: 'CIS',
                            marker: { colors:allColor[0]}
                        },

                    ]}
                layout={{
                    width: 500, 
                    height: 400, 
                    title:'THe Amout of students in Major CIS',
                    hovermode:'closest',
                }}
            />
            
            <Plot
                data={[
                    {
                        className:"pie",
                        values: allValues[1],
                        labels: allLable,
                        type:'pie',
                        name: 'ACC',
                        marker: { colors:allColor[1]},
                    },

                ]}
                layout={{
                    width: 500, 
                    height: 400, 
                    title:'THe Amout of students in Major ACC',
                    hovermode:'closest',
                }}
            />

            <Plot
                data={[
              
                    {
                        values: allValues[2],
                        labels: allLable,
                        type:'pie',
                        name: 'TR',
                        marker: { colors:allColor[2]},
                    },
                ]}
                layout={{
                    width: 500, 
                    height: 400, 
                    title:'THe Amout of students in Major TR',
                    hovermode:'closest',
                }}
            />

            <Plot
                data={[
                   
                    {
                        values: allValues[3],
                        labels: allLable,
                        type:'pie',
                        name: 'MG',
                        marker: { colors:allColor[3]},
                      
                    },

                ]}
                layout={{
                    width: 500, 
                    height: 400, 
                    title:'THe Amout of students in Major MG',
                    hovermode:'closest',
                }}
            />
        </div>
    )
}

export default PieCharts;