import React from 'react';
import Plot from 'react-plotly.js';

const BarCharts = () =>{
    return (
        <div className="polt">
              <Plot
                data={[
                    {
                        x: [1,2,3,4,5,6],
                        y: [22,23,25,22,25,22],
                        type:'bar',
                        marker:{color:'blue'},
                        name:'Age'
                    },
                    {
                        x: [1,2,3,4,5,6],
                        y: [53,49,63,50,72,69],
                        type:'bar',
                        marker:{color:'red'},
                        name:'Weight'
                    },
                    {
                        x: [1,2,3,4,5,6],
                        y: [155,150,155,156,160,162],
                        type:'bar',
                        marker:{color:'orange'},
                        name:'Height'
                    }

                ]}
                layout={{
                    width: '100%', 
                    height: '100%', 
                    title:'THe Growth of students',
                    hovermode:'closest',
                }}
            />
        </div>
    )
}

export default BarCharts;