import React from 'react';
import Plot from 'react-plotly.js';
import '../App.css';

const ShatterPolts = () =>{

    return(
    <div className="polt">
        <Plot 
            data={[
                {
                    x: [1, 2, 3,4,5,6],
                    y: [53,49,63,54,69,69],
                    type: 'scatter',
                    mode: 'markers',
                    marker: {color: 'red',size: 10},
                    name:'Weigth',
                    showlegend:true
                },
                {
                    x: [1, 2, 3,4,5,6],
                    y: [155,149,156,159,160,162],
                    type: 'scatter',
                    mode: 'markers',
                    marker: {color: 'blue',size: 10},
                    name:'Heigth',
                    showlegend:true
                },
                {
                    x: [1, 2, 3,4,5,6],
                    y: [22,23,24,22,24,22],
                    type: 'scatter',
                    mode: 'markers',
                    marker: {color: 'orange',size: 10},
                    name:'Age',
                    showlegend:true
                }
            ]}
            layout={{width: '100%', height: '100%', title: 'The Growth student'}}
      />
    </div>

    )
    
}

export default ShatterPolts;