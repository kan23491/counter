import React from 'react';
import Plot from 'react-plotly.js';

const LineCharts = () => {
    return(
        <div className="polt">
             <Plot
                data={[
                    {
                        x: [1,2,3,4,5,6],
                        y: [53,49,63,50,72,69],
                        type:'line',
                        marker:{color:'blue'},
                        name:'Age'
                    },
                    {
                        x: [1,2,3,4,5,6],
                        y: [22,23,25,22,25,22],
                        type:'line',
                        marker:{color:'red'},
                        name: 'weight'
                    },
                    {
                        x: [1,2,3,4,5,6],
                        y: [155,149,156,159,160,162],
                        type:'line',
                        marker:{color:'orange'},
                        name: 'heigth'
                    }
                ]}
                layout={{
                    width: '100%', 
                    height: '100%', 
                    title:'THe Growth of students',
                    hovermode:'closest',
                }}
            />
        </div>
    )
}

export default LineCharts;